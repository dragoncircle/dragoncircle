import { List, Map } from 'immutable';

const init = List([]);

export default function(messages=init, action) {
  switch(action.type) {
    case 'SEND_MESSAGE':
      return messages.push(Map(action.payload));
    case 'TOGGLE_MESSAGE':
      return messages.map(t => {
    if(t.get('id') === action.payload) {
      return t.update('isDone', isDone => !isDone);
    } else {
      return t;
    }
  });
    default:
      return messages;
  }
}
