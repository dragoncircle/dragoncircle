// succinct hack for generating passable unique ids
const uid = () => Math.random().toString(34).slice(2);

export function sendMessage(text) {
  return {
    type: 'SEND_MESSAGE',
    payload: {
      id: uid(),
      isDone: false,
      text: text
    }
  };
}

export function toggleMessage(id) {
  return {
    type: 'TOGGLE_MESSAGE',
    payload: id
  }
}
