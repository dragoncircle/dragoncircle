import React, { Component } from 'react';
import styled from 'styled-components'



export function ChatMessage(props) {
  const { message } = props;
  if(message.isDone) {
    return <strike>{message.text}</strike>;
  } else {
    return <span>{message.text}</span>;
  }
}

export function ChatBox(props) {
  const { messages, toggleMessage, sendMessage } = props;

  const onSubmit = (event) => {
    const input = event.target;
    const text = input.value;
    const isEnterKey = (event.which == 13);
    const isLongEnough = text.length > 0;

    if (isEnterKey && isLongEnough) {
      input.value = '';
      sendMessage(text);
    }
  }
  const toggleClick = id => event => toggleMessage(id);

  return (
    <div>
      <input type='text' placeholder='type message' onKeyDown={onSubmit}/>

        {messages.map(t => (
          <p key={t.get('id')} onClick={toggleClick(t.get('id'))}>
            <ChatMessage message={t.toJS()} />
          </p>

        ))}

    </div>
  );
}

