
import { connect } from 'react-redux';
import * as components from './ChatBox';
import { sendMessage, toggleMessage } from './ChatActions';


export const ChatBox = connect(

  function mapStateToProps(state) {
    return { messages: state };
  },
  function mapDispatchToProps(dispatch) {
    return {
      sendMessage: text => dispatch(sendMessage(text)),
      toggleMessage: id => dispatch(toggleMessage(id))
    };
  }
)(components.ChatBox);
