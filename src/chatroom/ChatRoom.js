import React, { Component } from 'react';
import styled from 'styled-components'
import { ChatBox } from './chatbox/ChatContainer';
import { List, Map } from 'immutable';

const ChatGrid = styled.div`
  padding: auto;
  margin: 0;
  background:grey;
  display: flex;
  height: 97%;
  width: 99%;
  position:absolute;
`;

const ChatColumn = styled.div`
  background: teal;
  margin: 0.1%;
  height:100%;
  margin-top: 10px;
	flex: ${props => props.flex};
  flex-direction:column;
`;

const VideoCard = styled.div`
  background: blue;
  height: 33%;
  flex:1;
  position:;
  margin:1px;
  padding:1px;
`;

export function ChatRoom(props){
    const { messages } = props;
    return (
      <ChatGrid>
        <ChatColumn flex={1}>
          <VideoCard>
            yo
          </VideoCard>
          <VideoCard>
            yo
          </VideoCard>
          <VideoCard>
            yo
          </VideoCard>


        </ChatColumn>
        <ChatColumn flex={2}>
        <ChatBox />
        </ChatColumn>
        <ChatColumn flex={1}>
          <VideoCard>
            yo
          </VideoCard>
          <VideoCard>
            yo
          </VideoCard>
          <VideoCard>
            yo
          </VideoCard>
        </ChatColumn>
      </ChatGrid>
  );
}

