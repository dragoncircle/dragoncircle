import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from './chatroom/chatbox/ChatReducer';
import { ChatRoom } from './chatroom/ChatRoom';

const store = createStore(reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);


ReactDOM.render(<Provider store={store}>
                  <ChatRoom />
                </Provider>,

                document.getElementById('root'));


registerServiceWorker();
